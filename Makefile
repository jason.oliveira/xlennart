CC = gcc
CFLAGS = -g -O2
CPPFLAGS = 
LDFLAGS = 
LIBS =    -lXm -lXaw -lXmu -lXt -lXpm -lX11 -lgtk-x11-2.0 -lgdk-x11-2.0 -lpangocairo-1.0 -latk-1.0 -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lpangoft2-1.0 -lpango-1.0 -lgobject-2.0 -lglib-2.0 -lharfbuzz -lfontconfig -lfreetype   -lSM -lICE
INSTALL=/usr/bin/install -c
INSTALL_DATA=${INSTALL} -m 644
MKINSTALLDIRS = $(SHELL) $(top_srcdir)/mkinstalldirs

X_CFLAGS=
GTK_CFLAGS=-I/usr/include/gtk-2.0 -I/usr/lib64/gtk-2.0/include -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/lib64/libffi/include -I/usr/include/harfbuzz -I/usr/include/freetype2 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/fribidi -I/usr/include/cairo -I/usr/include/libpng16 -I/usr/include/pixman-1 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/atk-1.0 -pthread 

prefix=/usr
exec_prefix=${prefix}
bindir=${exec_prefix}/bin
mandir=${prefix}/share/man
datadir=${prefix}/share
localstatedir=${prefix}/var
top_srcdir = .
srcdir = .


ALL_CFLAGS =	$(CFLAGS) $(CPPFLAGS) -I$(srcdir)
ALL_LDFLAGS =	$(CFLAGS) $(LDFLAGS)
IMAGE_DEFINES=-DIMAGES=\"${datadir}/xlennart\"
SCORE_DEFINES=-DSCOREFILE=\"${localstatedir}/games/xlennart.scores\"

PROG =	xlennart

OBJS =	Bill.o Bucket.o Cable.o Computer.o Game.o Horde.o Network.o \
	OS.o Scorelist.o Spark.o UI.o util.o  x11-motif.o x11-athena.o x11.o gtk.o

XPMS =	pixmaps/about.xpm pixmaps/billA_0.xpm \
	pixmaps/billA_1.xpm pixmaps/billA_10.xpm pixmaps/billA_11.xpm \
	pixmaps/billA_12.xpm pixmaps/billA_2.xpm pixmaps/billA_3.xpm \
	pixmaps/billA_4.xpm pixmaps/billA_5.xpm pixmaps/billA_6.xpm \
	pixmaps/billA_7.xpm pixmaps/billA_8.xpm pixmaps/billA_9.xpm \
	pixmaps/billD_0.xpm pixmaps/billD_1.xpm pixmaps/billD_2.xpm \
	pixmaps/billD_3.xpm pixmaps/billD_4.xpm pixmaps/billL_0.xpm \
	pixmaps/billL_1.xpm pixmaps/billL_2.xpm pixmaps/billR_0.xpm \
	pixmaps/billR_1.xpm pixmaps/billR_2.xpm pixmaps/bsd.xpm \
	pixmaps/bsdcpu.xpm pixmaps/bucket.xpm pixmaps/arch.xpm \
	pixmaps/icon.xpm pixmaps/centos.xpm pixmaps/logo.xpm \
	pixmaps/maccpu.xpm pixmaps/debian.xpm pixmaps/nextcpu.xpm \
	pixmaps/gentoo.xpm pixmaps/openbsd.xpm pixmaps/os2cpu.xpm \
	pixmaps/mandriva.xpm pixmaps/openbsd.xpm\
	pixmaps/spark_0.xpm pixmaps/slackware.xpm pixmaps/suse.xpm \
	pixmaps/spark_1.xpm pixmaps/ubuntu.xpm pixmaps/sgicpu.xpm \
	pixmaps/suncpu.xpm pixmaps/toaster.xpm pixmaps/initfail.xpm

XBMS =	bitmaps/arch.xbm bitmaps/bsd.xbm bitmaps/bucket.xbm bitmaps/centos.xbm \
	bitmaps/debian.xbm bitmaps/gentoo.xbm bitmaps/hand_down.xbm \
	bitmaps/hand_down_mask.xbm bitmaps/hand_up.xbm bitmaps/hand_up_mask.xbm \
	bitmaps/initfail.xbm bitmaps/mandriva.xbm bitmaps/openbsd.xbm \
	bitmaps/slackware.xbm bitmaps/suse.xbm bitmaps/ubuntu.xbm
	

MANDIR = man6
MAN = xlennart.6

all: ${PROG}

xlennart: ${OBJS}
	$(CC) $(ALL_LDFLAGS) -o $@ ${OBJS} ${LIBS}

.c.o:
	$(CC) $(ALL_CFLAGS) -c $< -o $@

Scorelist.o: Scorelist.c
	$(CC) $(ALL_CFLAGS) $(SCORE_DEFINES) -c $< -o $@

x11-motif.o: x11-motif.c
	$(CC) $(ALL_CFLAGS) $(X_CFLAGS) -c $< -o $@

x11-athena.o: x11-athena.c
	$(CC) $(ALL_CFLAGS) $(X_CFLAGS) -c $< -o $@

x11.o: x11.c
	$(CC) $(ALL_CFLAGS) $(X_CFLAGS) $(IMAGE_DEFINES) -c $< -o $@

gtk.o: gtk.c
	$(CC) $(ALL_CFLAGS) $(GTK_CFLAGS) $(IMAGE_DEFINES) -c $< -o $@

install:
	$(MKINSTALLDIRS) ${DESTDIR}${bindir}
	$(INSTALL) ${PROG} ${DESTDIR}${bindir}/${PROG}
	$(MKINSTALLDIRS) ${DESTDIR}${mandir}/${MANDIR}
	$(INSTALL_DATA) ${srcdir}/${MAN} ${DESTDIR}${mandir}/${MANDIR}
	$(MKINSTALLDIRS) ${DESTDIR}${localstatedir}/games
	$(INSTALL_DATA) ${srcdir}/scores ${DESTDIR}${localstatedir}/games/xlennart.scores.default
	$(MKINSTALLDIRS) ${DESTDIR}${datadir}/xlennart
	$(MKINSTALLDIRS) ${DESTDIR}${datadir}/xlennart/pixmaps
	$(MKINSTALLDIRS) ${DESTDIR}${datadir}/xlennart/bitmaps
	for i in ${XPMS}; do \
		${INSTALL_DATA} ${srcdir}/$$i ${DESTDIR}${datadir}/xlennart/pixmaps ; \
	done
	for i in ${XBMS}; do \
		${INSTALL_DATA} ${srcdir}/$$i ${DESTDIR}${datadir}/xlennart/bitmaps ; \
	done

uninstall:
	if [ -f ${DESTDIR}${bindir}/${PROG} ] ; \
	then \
		rm ${DESTDIR}${bindir}/${PROG} ; \
	fi;
	if [ -f ${DESTDIR}${mandir}/${MANDIR}/${MAN} ] ; \
	then \
		rm ${DESTDIR}${mandir}/${MANDIR}/${MAN} ; \
	fi;
	if [ -f ${DESTDIR}${localstatedir}/games/xlennart.scores.default ] ; \
	then \
		rm ${DESTDIR}${localstatedir}/games/xlennart.scores.default ; \
	fi;
	if [ -d ${DESTDIR}${datadir}/xlennart ] ; \
	then \
		rm -r ${DESTDIR}${datadir}/xlennart ; \
	fi;

distclean::
	rm -f config.cache config.h config.log config.status Makefile

distclean clean::
	rm -f ${PROG} *.o
rm -f *core core*:
