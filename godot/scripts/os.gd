extends Node
# Turn this into a dictionary, and make the value an array.
# pixmap location, lennart_resistance, leah_resistance, bill_resistance, repair_cost
var distros: Dictionary = {
	"initfail":["res://godot/pixmaps/os/initfail0.png", 1, 1, 1, 10],
	"almalinux":["res://godot/pixmaps/os/almalinux.png", 1, 3, 3, 5],
	"alpine":["res://godot/pixmaps/os/alpine.svg", 7, 5, 7, 4],
	"antix":["res://godot/pixmaps/os/antix.png", 7, 1, 7, 4],
	"arch":["res://godot/pixmaps/os/arch.svg", 1, 3, 6, 5],
	"artix":["res://godot/pixmaps/os/artix.svg", 7, 7, 7, 5],
	"bsd":["res://godot/pixmaps/os/freebsd.svg", 10, 2, 6, 6],
	"calculate":["res://godot/pixmaps/os/calculate.png", 9, 9, 9, 7],
	"chimera":["res://godot/pixmaps/os/chimera.png"],
	"crux":["res://godot/pixmaps/os/crux.png", 9, 3, 8, 9],
	"debian":["res://godot/pixmaps/os/debian.svg", 1, 1, 6, 3],
	"devuan":["res://godot/pixmaps/os/devuan.png", 9, 8, 9, 3],
	"fedora":["res://godot/pixmaps/os/fedora.svg", 1, 1, 3, 5],
	"gentoo":["res://godot/pixmaps/os/gentoo.svg", 5, 3, 8, 8],
	"guixsd":["res://godot/pixmaps/os/guixsd.svg", 9, 1, 9, 7],
	"kaios":["res://godot/pixmaps/os/kaios.svg", 1, 7, 9, 4],
	"kisslinux":["res://godot/pixmaps/os/kisslinux.png", 9, 9, 9, 8],
	"linuxmint":["res://godot/pixmaps/os/linuxmint.svg", 2, 4, 6, 3],
	"mageia":["res://godot/pixmaps/os/mageia.svg", 8, 6, 8, 6],
	"manjaro":["res://godot/pixmaps/os/manjaro.svg", 1, 4, 6, 4],
	"mxlinux":["res://godot/pixmaps/os/mxlinux.svg", 9, 4, 8, 4],
	"nitrux":["res://godot/pixmaps/os/nitrux.svg", 9, 3, 8, 4],
	"openbsd":["res://godot/pixmaps/os/openbsd.svg", 10, 10, 10, 9],
	"opensuse":["res://godot/pixmaps/os/opensuse.svg", 1, 3, 5, 5],
	"pclinuxos":["res://godot/pixmaps/os/pclinuxos.svg", 9, 7, 7, 8],
	"popos":["res://godot/pixmaps/os/popos.svg", 1, 6, 7, 3],
	"slackware":["res://godot/pixmaps/os/slackware.svg", 10, 10, 10, 4],
	"ubuntu":["res://godot/pixmaps/os/ubuntu.svg", 1, 4, 6, 3],
	"venom":["res://godot/pixmaps/os/venom.jpg", 9, 8, 9, 7],
	"void":["res://godot/pixmaps/os/void.svg", 10, 7, 10, 7],
}

func os_randpc():
	return distros[randi_range(0, distros.size() - 1)]
