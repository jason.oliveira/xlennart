# Roadmap for XLennart to 2.0 and beyond

### v1.1.2:
* bugfixes and updates 
* Godot current progress

### v1.3.0: 
* XBill Parity in Godot

### v1.4.0:
* XLennart Parity in Godot
* reorganization of source tree.

### v1.9.0:
* XLennart GTK2/XAW path renamed "XLennart, Part One".
* Godot path renamed to "XLennart, Part Two".