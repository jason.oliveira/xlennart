 
# XLennart v1.9.0 - Bill Strikes Back.

# Story so far:
* Bill lost the battle of the server room (XBill)
* Bill regroups and creates the Azure and CBL-Mariner viruses.
* Bill makes alliance with Department of Government Pigs, can surveil.
* Bill forms the `0x0` Squad, funding sleeper agents to infect from within.
* Bill hires Lennart to write malware.   
* Bill hires Leah to write CoC.   

The Diabolical hacker known only by the handle of "Bill" was coordinating with the evil hacker, codename: "Lennart", the entire time! "Lennart" kept you distracted while he conquered the government! 

The evil computer hackers have organized, and the fate of the world rests in your hands! The hacker known as "Bill" has taken over the government pigs, who are now under his direction! 

# Villains: 
## Lennart
Voice Samples to record:    
- [ ] Why do you hate disabled people?
- [ ] Resolved wontfix


### Powers:    
**systemd** - repair cost x3    
* If systemd is installed, udev is always infected. repair cost 1.1x
* gummiboot needs to be replaced with grub - upgrade path?    

**BUS1** - repair cost x5    
**PulseAudio** -    
**NetworkManager** - Viruses spread 3x faster.

## Leah
- Air-based, rainbow insect wings.    
- Mimic behavior of Mario Paint flies.    
- If you swat her, two or three mini-Leahs appear in her place.    

### Powers:    
**CodeOfConduct** - slows mouse speed.

## Mini-Leah
### powers:    
**ForceMultiplier** - connect to infected network cable, increase `infection_rate`

## Government Pig
- teeny-tiny and adorable.
- dozens of them at a time.
### Powers:    
**Swarm:** Pigs always appear in large numbers.   
**Appropriate**: Steal devices and walk away with them. Device is lost if boundary is reached.    
**Surveillance**: slow network traffic, or make device vulnerable to poetterware.

## Bill
### Voice samples to record:    
- [ ] If we do a really great job, we can murder millions!
- [ ] Innovative!
- [ ] 640K should be enough for your doom!    

### Powers:    
**Fear** - mouse avoids Bill - cannot click.     
**Uncertainty** - Unable to see infected devices or cables for a short time. All screens turn black.  
**Doubt** - accurate clicks reduced to 30% accuracy.   
**Proxy war** - Summon Lennart or Leah    
**Surveillance** - Summon Men in Black    
**Quadrillionaire's Reach** - attempt to close the window of the game.

# Devices:
### Servers
* Less vulnerable to poetterware
* More vulnerable to Mini-Leah
### Desktops
* More vulnerable to poetterware
### Laptops
* More vulnerable to poetterware
### Phones
* Less vulnerable to poetterware
### Switches
### WAPs
### Game Consoles
### Voting Machines

~~`.`~~
# Gameplay Changes:
* more unique devices, 
* new ways to connect devices.
* servers can be disconnected.
* Villains that can assist each other.

~~`.`~~

# Draft Legal Notice:    
These electronic files included alongside this "Work", as defined by the GNU General Public License v2, as well as the Work itself in a compiled or executable form, is a transformative work of parody, and all likenesses, samples, quotations, misquotes, and other forms of caricaturization of public figures based on public opinion or perception, living or dead, are protected under Fair Use, and said specific forms of caricaturization aforementioned are claimed as the Intellectual Property of the authors of this work, all rights reserved.
